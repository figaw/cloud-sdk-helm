# Change namespace. Usage: ns kube-system
function ns() {
    kubectl config set-context "$( kubectl config current-context )" --namespace="$1"
}

# Get credentials for gcloud cluster. Usage get-credentials my-cluster
alias get-credentials="gcloud container clusters get-credentials"

## Kubernetes
alias k="kubectl"
alias describe="kubectl describe"
alias logs="kubectl logs"

# Pods
alias pods="kubectl get pods"
alias pods-all="kubectl get pods --all-namespaces"
alias podsw="kubectl get pods -o wide"
alias podsw-all="kubectl get pods -o wide --all-namespaces"

# Services
alias svcs="kubectl get svc"
alias svcs-all="kubectl get svc --all-namespaces"
alias svcsw="kubectl get svc -o wide"
alias svcsw-all="kubectl get svc -o wide --all-namespaces"

# Nodes
alias nodes="kubectl get nodes"
alias nodesw="kubectl get nodes -o wide"

echo "-- profile loaded --"
