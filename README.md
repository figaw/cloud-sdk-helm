# cloud-sdk-helm

## Prerequisites

- Requires image from [figaw/cloud-sdk](https://github.com/figaw/cloud-sdk) to build
- A `helm-<some version>-linux-amd64.tar.gz` in the build context

## Build

> overwrite the `helm-v2.12.3-linux-amd64.tar.gz` in the Dockerfile
> with your preferred version of Helm.

```shell
$ docker build -t figaw/cloud-sdk-helm:234.0.0 .
...
Successfully tagged figaw/cloud-sdk-helm:234.0.0
```

## Run

```shell
docker run --rm -ti \
    --volumes-from gcloud-container \
    -e CLOUDSDK_CORE_PROJECT \
    -e CLOUDSDK_COMPUTE_ZONE \
    -e CLOUDSDK_COMPUTE_REGION \
    -v "$PWD":/non-privileged/ figaw/cloud-sdk-helm:234.0.0
```

NB: mount your current working directory into the container, if you want a lasting session.

## Helm init - Getting Started On

### GKE

RBAC security, see: [Role-based Access Control](https://github.com/helm/helm/blob/master/docs/rbac.md)

## Known Issues

The path will be off, so add `/google-cloud-sdk/bin` to the path.
