FROM alpine as builder

ADD helm-v2.12.3-linux-amd64.tar.gz /root/

FROM figaw/gcloud-sdk:234.0.0

COPY --from=builder /root/linux-amd64 /usr/local/bin
COPY .profile /non-privileged/.profile

WORKDIR /non-privileged

CMD [ "bash", "--login" ]
